/**
 * @file fonction.h
 * @author Louis LABORIE && Alexis LAURENT && Sami GHEBRID && Ana DE AMORIN
 * @brief Contient les algorithmes de la SAE 1.01
 * @date 2022-11-10
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Fonction chargeant les données du fichier 'adherent.txt' dans les tableaux passés en paramètre.
 * 
 * @param tabAdherent tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param tphys taille physique des tableaux
 * @return la taille logique des tableaux
 */

int chargementAdherent(int tabAdherent[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tphys);

/**
 * @brief Fonction chargeant les données du fichier 'activite.txt' dans les tableaux passés en paramètre.
 * 
 * @param tabPoint tableau des points des différentes activités
 * @param tabNbEntree tableau du nombre d'entrées des différentes activités
 * @param tphys taille physique des tableaux
 * @return la taille logique des tableaux
 */
int chargementActivite(int tabPoint[],int tabNbEntree[], int tphys);

/**
 * @brief Fonction rechercheant dans un tableau d'entiers trié le contenu de la variable 'val' passé en paramètre.
 * 
 * @param tabTri tableau d'entiers trié 
 * @param val valeur recherchée
 * @param nb taille logique du tableau
 * @param trouve variable pointant vers un entier permettant de savoir si la valeur a été trouvée.
 * @return la position d'insertion dans le tableau 
 */
int recherche(int tabTri[], int val, int nb, int *trouve);

/**
 * @brief Décale vers la droite d'un cran toutes les valeurs du tableau.
 * 
 * @param tabTri Le tableau d'entiers dont les valeurs vont être décalées
 * @param nb taille logique du tableau
 * @param pos indice dans le tableau à partir de laquelle on décale les valeurs.
 */
void decalageDroite(int tabTri[], int nb, int pos);
/**
 * @brief Décale vers la gauche d'un cran toutes les valeurs du tableau.
 * 
 * @param tabTri Le tableau d'entiers dont les valeurs vont être décalées
 * @param nb taille logique du tableau
 * @param pos indice dans le tableau à partir de laquelle on décale les valeurs.
 */
void decalageGauche(int tabTri[], int nb, int pos);
/**
 * @brief Fonction appelée par la fonction 'ajouterAdherent' permettant de décaler tout les éléments des tableaux d'un cran vers la droite.
 * 
 * @param tabAdherent tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param nb taille logique des tableaux
 * @param val valeur de la nouvelle carte(adherent)
 * @param tmax taille physique des tableaux
 * @return position d'insertion dans les tableaux
 */
int ajouter(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int val, int tmax);

/**
 * @brief Fonction permettant d'ajouter un nouvel adhérent.
 * 
 * @param tabAdherent tableaux des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param nb taille logique des tableaux
 * @param val valeur de la nouvelle carte(adherent)
 * @param tmax taille physique des tableaux
 * @return nouvelle taille logique des tableaux 
 */
int ajouterAdherent(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int tmax);

/**
 * @brief Fonction appelée par 'supprimerAdherent' permettant de décaler tout les éléments des tableaux d'un cran vers la gauche
 * 
 * @param tab tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param nb taille logique des tableaux
 * @param val valeur de la nouvelle carte(adherent)
 * @param tmax taille physique des tableaux
 * @return nouvelle taille logique des tableaux
 */
int suppression(int tab[], int tabPoints[], int jour[], int mois[], int annee[], int carte[],int nb, int val);

/**
 * @brief Affiche toutes les informations sur tout les adhérents
 * 
 * @param tabNumCarte tableau des adhérents
 * @param tabPoint tableau des points
 * @param carte tableau du statut des cartes
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param taille taille logique des tableaux
 */
void affichageTous(int tabNumCarte[], int tabPoint[], int carte[], int jour[], int mois[], int annee[],int taille);

/**
 * @brief Fonction appelée par la fonction 'gestionCarte' qui permet d'afficher les informations reliées à la carte numéro 'val'.
 * 
 * @param tabAdhe tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param nb taille logique des tableaux
 * @param val numéro de la carte(adhérent)
 */
void affichageCarte(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int nb, int val);

/**
 * @brief Fonction qui affiche les informations de l'activité numéro 'val'
 * 
 * @param tabCoutPoint tableau du cout en points des différentes activités
 * @param tabNbEntree tableau du nombre d'entrée des différentes activités
 * @param tailleL taille logique des tableaux
 */
void affichageActivite(int tabCoutPoint[], int tabNbEntree[], int tailleL);

/**
 * @brief Fonction qui affiche toutes les informations sur toutes les activités
 * 
 * @param tabCoutPoint tableau du cout en points des différentes activités
 * @param tabNbEntree tableau du nombre d'entrée des différentes activités
 * @param tailleL taille logique des tableaux
 */
void affichageToutActivite(int tabCoutPoint[], int tabNbEntree[], int tailleL);


/**
 * @brief Fonction qui crédite le nombre de points d'une carte d'adhérent donnée.
 * 
 * @param tabAdherent tableau des adhérents
 * @param tabPoint tableau des points des adhérents
 * @param nb taille logique des tableaux
 */
void alimenterCarte (int tabAdherent[], int tabPoint[],int nb);
/**
 * @brief Ajoute une activité.
 * 
 * @param tabCoutPoint tableau du cout en points des différentes activités 
 * @param tailleL taille logique des tableaux
 */
void ajoutActivite(int tabCoutPoint[], int tailleL);

/**
 * @brief Supprime une activité choisie.
 * 
 * @param tabCoutPoint tableau du cout en points des différentes activités
 * @param tabNbEntree tableau du nombre d'entrées des différentes activités
 * @param tailleL taille logique des tableaux
 */
void suppActivite(int tabCoutPoint[], int tabNbEntree[],int tailleL);

/**
 * @brief Permet de soustraire à un adhérent le nombre de points correspondant à l'activté qu'il a choisi.
 * 
 * @param tabAdherent tableau des adhérents
 * @param tabPoint tableau des points des adhérents
 * @param tabCoutPoint tableau du cout en points des différentes activités
 * @param tabNbEntree tableau du nombre d'entrées des différentes activités
 * @param tailleLAdhe taille logique des adhérents
 * @param tailleLAct taille logique des activités
 */
void faireActivite(int tabAdherent[], int tabPoint[],int tabCoutPoint[], int tabNbEntree[],int carte[], int tailleLAdhe, int tailleLAct);
/**
 * @brief Permet de sauvegarder les données des tableaux passés en paramètre dans le fichier 'adherent.txt'
 * 
 * @param tabAdherent tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param tailleL taille logique des tableaux
 * @return Code de vérification : 0 si réussite, -1 si échec
 */
int SauvegardeAdherent(int tabAdherent[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL);
/**
 * @brief Sauvegarde les données des tableaux passés en paramètre dans le fichier 'activite.txt'
 * 
 * @param tabCoutPoint tableau du cout en points des différentes activités
 * @param tabNbEntree tableau du nombre d'entrées des différentes activités
 * @param tailleL taille logique des tableaux
 * @return Code de vérification : 0 si réussite, -1 si échec
 */
int SauvegardeActivite(int tabCoutPoint[],int tabNbEntree[],int tailleL);
/**
 * @brief Permet de supprimer un adherent.
 * 
 * @param tabAdhe tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param tailleL taille logique des tableaux
 * @return la nouvelle taille logique des tableaux
 */
int supprimerAdherent(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL);

/**
 * @brief Permet de modifier une activité choisie.
 * 
 * @param tabCoutPoint tableau du cout en points des différentes activités
 * @param tailleL taille logique des tableaux
 */
void modifActivite(int tabCoutPoint[],int tailleL);
/**
 * @brief Activer/Désactiver une carte donnée (paramètre)
 * 
 * @param tabAdhe tableau des adhérents
 * @param carte tableau du statut des cartes
 * @param tailleL taille logique des tableaux
 * @param val valeur de la carte(adhérent)
 * @return Code de vérification : 0 si réussite, -1 si échec
 */
int activerCarte(int tabAdhe[], int carte[], int tailleL, int val);
/**
 * @brief Fonction qui globale qui permet de traiter une carte choisie.
 * 
 * @param tabAdhe tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param tailleL taille logique des tableaux
 */
void gestionCarte(int tabAdhe[], int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tailleL);

/**
 * @brief Fonction globale qui gère les deux sauvegardes.
 * 
 * @param tabAdhe tableau des adhérents
 * @param tabPoints tableau des points des adhérents
 * @param jour tableau des jours de la date d'inscription
 * @param mois tableau des mois de la date d'inscription
 * @param annee tableau des années de la date d'inscription
 * @param carte tableau du statut des cartes
 * @param tabCoutPoint tableau du cout en points des différentes activités
 * @param tabNbEntree tableau du nombre d'entrées des différentes activités
 * @param tailleAdhe taille logique du tableau des adhérents
 * @param tailleAct taille logique du tableau des activités
 * @return 0 
 */
int Sauvegarde(int tabAdhe[],int tabPoints[], int jour[], int mois[], int annee[], int carte[], int tabCoutPoint[], int tabNbEntree[], int tailleAdhe, int tailleAct);

/**
 * @brief Fonction globale qui gère l'affichage des différents menus.
 * 
 */
void GestionSalle(void);

/**
 * @brief Affiche le menu
 * 
 * @return 0
 */
int Menu(void);


/**
 * @brief Permet d'effacer le contenu du terminal
 * 
 */
void clearpage(void);